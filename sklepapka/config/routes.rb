Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root "pages#show", page: "home"
  get "/pages/:page" => "pages#show"
    get 'index' => "orders#index"
  get "orders/new"
  get "clients/new" => "clients#new"
  get 'index' => "clients#index"

  
resources :orders  
resources :clients

end
