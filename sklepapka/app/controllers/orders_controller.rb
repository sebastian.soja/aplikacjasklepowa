class OrdersController < ApplicationController


    def index
        @orders = Order.all
    end

    def show
        @order= Order.find(params[:id])
    end

    def new
        @order= Order.new
    end

    def create
        @order= Order.new(order_params)
        if @order.save
            redirect_to orders_path
        else
            redirect_to new_order_path
    end
end
    def edit
        @order= Order.find(params[:id])
    end

    def update
        @order= Order.find(params[:id])

        if @order.update(order_params)
            redirect_to orders_path
        else
            redirect_to new_order_path
    end
end

    def destroy
        @order= Order.find(params[:id])

        @order.destroy
            redirect_to order_path, :notice => "Order has been deleted"
        
            
         
     end

    private
   def order_params
    params.require(:order).permit(:order_id, :client_id,:deliever_id, :name, :phone, :address, :payment_option, :amount, :order_status) 

   
    end

end
    
