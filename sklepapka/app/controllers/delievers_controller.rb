class DelieversController < ApplicationController


    def index
        @deleviers = Delevier.all
    end

    def show
        @delevier= Delevier.find(params[:id])
    end

    def new
        @delevier= Delevier.new
    end

    def create
        @delevier= Delevier.new(delevier_params)
        if @delevier.save
            redirect_to delevier_path
        else
            redirect_to new_delevier_path
    end
end
    def edit
        @delevier= Delevier.find(params[:id])
    end

    def update
        @delevier= Delevier.find(params[:id])

        if @delevier.update(delevier_params)
            redirect_to delevier_path
        else
            redirect_to new_delevier_path
    end
end

    def destroy
        @delevier= Delevier.find(params[:id])

        @delevier.destroy
            redirect_to delevier_path, :notice => "Delevier has been deleted"
        
            
         
     end

    private
   def delevier_params
    params.require(:delevier).permit(:delevier_id, :name, :adress, :phone_number, :comment)

   
    end

end
    
