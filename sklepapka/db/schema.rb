# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20191108190702) do

  create_table "category", primary_key: "category_id", force: :cascade do |t|
    t.nchar "name",        limit: 100
    t.nchar "description", limit: 300
  end

  create_table "clients", primary_key: "client_id", force: :cascade do |t|
    t.nchar   "name",         limit: 100
    t.nchar   "adress",       limit: 200
    t.nchar   "phone_number", limit: 20
    t.nchar   "comment",      limit: 100
    t.integer "order_id",     limit: 4
  end

  create_table "clients_orders", id: false, force: :cascade do |t|
    t.integer "order_id",  limit: 4, null: false
    t.integer "client_id", limit: 4, null: false
  end

  add_index "clients_orders", ["client_id", "order_id"], name: "index_clients_orders_on_client_id_and_order_id"
  add_index "clients_orders", ["order_id", "client_id"], name: "index_clients_orders_on_order_id_and_client_id"

  create_table "delievers", primary_key: "deliever_id", force: :cascade do |t|
    t.nchar "name",         limit: 100
    t.nchar "adress",       limit: 200
    t.nchar "phone_number", limit: 20
    t.nchar "comment",      limit: 100
  end

  create_table "dishes", primary_key: "dish_id", force: :cascade do |t|
    t.nchar "name",        limit: 100
    t.nchar "description", limit: 300
    t.float "price"
    t.nchar "comment",     limit: 100
  end

  create_table "dishes_category", primary_key: "dish_id", force: :cascade do |t|
    t.integer "category_id", limit: 4, null: false
  end

  create_table "dishes_orders", primary_key: "order_id", force: :cascade do |t|
    t.integer "dish_id", limit: 4, null: false
  end

  create_table "dishes_products", primary_key: "dish_id", force: :cascade do |t|
    t.integer "product_id", limit: 4, null: false
  end

  create_table "orders", primary_key: "order_id", force: :cascade do |t|
    t.nchar      "order_status",   limit: 50
    t.datetime   "order_date"
    t.datetime   "deliever_date"
    t.boolean    "credit_card"
    t.nchar      "comment",        limit: 100
    t.integer    "client_id",      limit: 4,          null: false
    t.integer    "deliever_id",    limit: 4,          null: false
    t.integer    "amount",         limit: 4
    t.integer    "phone",          limit: 4
    t.text_basic "address",        limit: 2147483647
    t.char       "payment_option", limit: 1
    t.char       "name",           limit: 100
  end

  create_table "posts", force: :cascade do |t|
    t.string   "title",      limit: 4000
    t.text     "body",       limit: 2147483647
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  create_table "products", primary_key: "product_id", force: :cascade do |t|
    t.nchar   "name",     limit: 100
    t.integer "quantity", limit: 4
    t.float   "price"
    t.nchar   "comment",  limit: 100
  end

  create_table "products_supplies", primary_key: "supply_id", force: :cascade do |t|
    t.integer "product_id", limit: 4, null: false
  end

  create_table "suppliers", primary_key: "supplier_id", force: :cascade do |t|
    t.nchar "name",         limit: 100
    t.nchar "adress",       limit: 200
    t.nchar "phone_number", limit: 20
    t.nchar "comment",      limit: 100
  end

  create_table "supplies", primary_key: "supply_id", force: :cascade do |t|
    t.datetime "order_date"
    t.datetime "deliever_date"
    t.nchar    "comment",       limit: 100
    t.integer  "supplier_id",   limit: 4,   null: false
  end

  create_table "sysdiagrams", primary_key: "diagram_id", force: :cascade do |t|
    t.string  "name",         limit: 128,        null: false
    t.integer "principal_id", limit: 4,          null: false
    t.integer "version",      limit: 4
    t.binary  "definition",   limit: 2147483647
  end

  add_index "sysdiagrams", ["principal_id", "name"], name: "UK_principal_name", unique: true

  add_foreign_key "dishes_category", "category", primary_key: "category_id", name: "relation_4_category_fk"
  add_foreign_key "dishes_category", "dishes", primary_key: "dish_id", name: "relation_4_dishes_fk"
  add_foreign_key "dishes_orders", "dishes", primary_key: "dish_id", name: "relation_3_dishes_fk"
  add_foreign_key "dishes_orders", "orders", primary_key: "order_id", name: "relation_3_orders_fk"
  add_foreign_key "dishes_products", "dishes", primary_key: "dish_id", name: "relation_6_dishes_fk"
  add_foreign_key "dishes_products", "products", primary_key: "product_id", name: "relation_6_products_fk"
  add_foreign_key "orders", "clients", primary_key: "client_id", name: "orders_clients_fk"
  add_foreign_key "orders", "delievers", primary_key: "deliever_id", name: "orders_delievers_fk"
  add_foreign_key "products_supplies", "products", primary_key: "product_id", name: "relation_7_products_fk"
  add_foreign_key "products_supplies", "supplies", primary_key: "supply_id", name: "relation_7_supplies_fk"
  add_foreign_key "supplies", "suppliers", primary_key: "supplier_id", name: "supplies_suppliers_fk"
end
